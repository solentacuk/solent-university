var gulp         = require('gulp'),
    sass         = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    concat       = require('gulp-concat'),
    uglify       = require('gulp-uglify'),
    rename       = require('gulp-rename'),
    browserSync  = require('browser-sync'),
    inject       = require('gulp-inject');

gulp.task('build-css', function() {
  return gulp.src('src/scss/**/*.scss')
  .pipe(sass({ outputStyle: 'compressed' })
  .on('error', sass.logError))
  .pipe(autoprefixer())
  .pipe(rename({ suffix: '.min' }))
  .pipe(gulp.dest('./public/css'))
  .pipe(browserSync.stream());
});

gulp.task('build-js', function() {
  return gulp.src([
    'src/js/vendors/jquery.jscrollpane.js',
    'src/js/vendors/jquery.magnificpopup.js',
    'src/js/vendors/jquery.mousewheel.js',
    'src/js/vendors/jquery.shuffle.js',
    'src/js/vendors/jquery.sticky-kit.js',
    'src/js/vendors/jquery.waypoints.js',
    'src/js/vendors/jquery.vide.js',
    'src/js/vendors/imgLiquid.js',
    'src/js/vendors/imagesloaded.pkgd.min.js',
    'src/js/vendors/progressbar.js',
    'src/js/vendors/rem.js',
    'src/js/vendors/respond.src.js',
    'src/js/vendors/foundation/foundation.js',
    'src/js/vendors/foundation/app.js',
    'src/js/vendors/cookiebar.js',
    'src/js/app.js'
  ])
  .pipe(concat('app.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('./public/js/min'));
});

gulp.task('js-watch', ['build-js'], browserSync.reload);

gulp.task('serve', ['build-css', 'build-js'], function() {
  browserSync.init({
    server: {
      baseDir: "./public"
    }
  });
  gulp.watch('./src/scss/**/*.scss', ['build-css']);
  gulp.watch('./src/js/**/*.js', ['js-watch']);
  gulp.watch('./public/**/*.html', browserSync.reload);
});

gulp.task('default', ['build-css', 'build-js', 'serve'], browserSync.reload);
