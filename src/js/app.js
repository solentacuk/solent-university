var SolentUI = (function($) {
  'use strict';

  // Private alias to settings
  var s;

  return {
    settings: function() {
      this.$window            = $(window);
      this.$body              = $('body');
      this.$timeline          = $('.timeline');
      this.$fix               = $('.fix');
      this.$pinned            = $('.pinned');
      this.$testimonials      = $('.testimonials');
      this.$imgLiquid         = $('.imgLiquid');
      this.$filter            = $('.filter');
      this.$filterControls    = $('.filter').find('.filter__item');
      this.$filterShuffle     = $('.js-filter-shuffle');
      this.$shuffleCols       = $('.js-shuffle-cols');
      this.$filterMatched     = false;
      this.toggleFiltersPanel = null;
    },

    init: function() {
      s = new this.settings();
      this.carouselShadow();
      this.carouselScrollPane();
      this.timeLine();
      this.testimonials();
      this.imgLiquid();
      this.stickyNav();
      this.progressBar();
      this.magnificPopup();
      this.filtering();
      this.shuffleCols();
      this.alternateSection();
      this.menuToggle();
      this.compareBar();
      this.smoothScrollTop();
      this.groupFilter();
      this.stickyColumn();
      this.moveNewsSidebar();
      this.bindUIActions();
      this.bindWindowActions();
    },

    isEven: function(num) {
      return num % 2 == 0;
    },

    debounce: function(func, wait, immediate) {
      var timeout;

      return function() {
        var context = this,
            args = arguments;

        var later = function() {
          timeout = null;
          if (!immediate) func.apply(context, args);
        };

        var callNow = immediate && !timeout;

        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    },

    timeLine: function() {
      s.$timeline.each(function() {
        var $timeline   = $(this),
            $item       = $timeline.find('.timeline__item'),
            activeClass = 'timeline__item--active';

        // Show the active info
        $timeline.find('.' + activeClass).find('.timeline__info').css('display', 'block');

        // Disable title links
        $item.find('.timeline__title a').on('click', function(e){ e.preventDefault(); });

        // On clicking the timeline item
        $item.on('click', function() {
          var $this   = $(this),
              $images = $timeline.parent().next().find('.timeline__img');

          if (!$this.hasClass(activeClass)) {
            // Close any active items
            $item.find('.timeline__info').stop(true, true).slideUp().parent().removeClass(activeClass);

            // Make this one active
            $this.find('.timeline__info').stop(true, true).slideDown().parent().addClass(activeClass);

            // If we have images
            if($images) {
              var activeNum     = $this.index() + 1,
                  $current      = $images.filter('.timeline__img--active'),
                  $currentPos   = $current.position(),
                  $activeImg    = $images.parent().find('.timeline__img[data-session=' + activeNum + ']'),
                  $activeImgPos = $activeImg.position();

              // If the session has an img show it else hide it
              if($activeImg.length) {
                $current.css({
                  top: $activeImgPos.top,
                  left: $activeImgPos.left
                }).removeClass('timeline__img--active').removeClass('timeline__img--disabled');

                $activeImg.css({
                  top: $currentPos.top,
                  left: $currentPos.left
                }).addClass('timeline__img--active');
              } else {
                $current.addClass('timeline__img--disabled');
              }
            }
          }
        });
      });
    },

    carouselShadow: function() {
      var $carouselSH = $('.carousel');

      if ($carouselSH.length) {
        $carouselSH.each(function() {
          var $this = $(this),
              totalWidthSH = 0;

          calculateWidthSH();

          $(window).resize(function() {
            calculateWidthSH();
            totalWidthSH = 0;
          });

          function calculateWidthSH() {
            // Calculate the width
            $this.find('.carousel__panel').each(function() {
              totalWidthSH += $(this).outerWidth(true);
            });

            // Apply the shadow class
            if($this.find('.carousel__column').width() <= totalWidthSH){
              $this.find('.carousel__column').addClass("carousel__column--shadow");
            }
            else {
              $this.find('.carousel__column').removeClass("carousel__column--shadow");
            }
          }
        });
      }
    },

    carouselScrollPane: function() {
      var $carousel = $('.carousel');

      if ($carousel.length) {

        $carousel.each(function() {
          var $this = $(this),
              totalWidth = 0;

          calculateWidth();

          $(window).resize(function() {
            calculateWidth();
            $('.scroll-pane').jScrollPane();
          });

          function calculateWidth() {
            // Calculate the width
            $this.find('.carousel__panel').each(function() {
              totalWidth += $(this).outerWidth(true);
            });

            // Apply the width
            $this.find('.carousel__container').width(totalWidth);
          }
        });

        // Apply the scroll pane
        setTimeout(function() {
          $('.scroll-pane').jScrollPane();
        }, 100);
      }
    },

    googleMaps: function() {
      var $mapCanvas = $('#map__canvas'),
          latLng     = $mapCanvas.data('lat-lng'),
          mapStyles  = null;

      // If no latLng specified return
      if (!latLng) {
        return;
      }

      // Set our map options
      var mapOptions = {
        center: new google.maps.LatLng(latLng[0], latLng[1]),
        zoom: 15,
        disableDefaultUI: false,
        mapTypeControl: false,
        scrollwheel: false,
        draggable: (Modernizr.touchevents ? false : true), // Enable draggable if were not on a touch screen
        disableDoubleClickZoom: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      // Set our styles
      mapStyles = [{
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [{
          "visibility": "simplified"
        }, {
          "hue": "#0066ff"
        }, {
          "saturation": 74
        }, {
          "lightness": 100
        }]
      }, {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [{
          "visibility": "simplified"
        }]
      }, {
        "featureType": "road",
        "elementType": "all",
        "stylers": [{
          "visibility": "simplified"
        }]
      }, {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [{
          "visibility": "off"
        }, {
          "weight": 0.6
        }, {
          "saturation": -85
        }, {
          "lightness": 61
        }]
      }, {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [{
          "visibility": "on"
        }]
      }, {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "road.local",
        "elementType": "all",
        "stylers": [{
          "visibility": "on"
        }]
      }, {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [{
          "visibility": "simplified"
        }]
      }, {
        "featureType": "water",
        "elementType": "all",
        "stylers": [{
          "visibility": "simplified"
        }, {
          "color": "#5f94ff"
        }, {
          "lightness": 26
        }, {
          "gamma": 5.86
        }]
      }];

      // Create our map
      var map = new google.maps.Map(document.getElementById('map__canvas'), mapOptions);

      // Apply our styles
      map.setOptions({
        styles: mapStyles
      });

      // Create our marker
      var mapIcon = {
        url: (window.devicePixelRatio > 1.5) ? $mapCanvas.data('marker-2x') : $mapCanvas.data('marker'),
        size: new google.maps.Size(82, 116),
        scaledSize: new google.maps.Size(41, 58),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(41, 41)
      };

      var mapMarker = new google.maps.Marker({
        position: new google.maps.LatLng(latLng[0], latLng[1]),
        map: map,
        icon: mapIcon
      });

      google.maps.event.addListener(map, 'mousedown', function() {
        SolentUI.googleMapsEnableScrollwheel(map);
      });

      s.$body.on('mousedown', function(event) {
        var clickedInsideMap = $(event.target).parents('#map__canvas').length > 0;

        if (!clickedInsideMap) {
          SolentUI.googleMapsDisableScrollwheel(map);
        }
      });

      s.$window.on('scroll', function() {
        SolentUI.googleMapsDisableScrollwheel(map);
      });
    },

    googleMapsEnableScrollwheel: function(map) {
      map.setOptions({
        scrollwheel: true
      });
    },

    googleMapsDisableScrollwheel: function(map) {
      map.setOptions({
        scrollwheel: false
      });
    },

    testimonials: function() {
      s.$testimonials.each(function(){
        var $orbit  = $(this).find('.orbit'),
            $images = $orbit.find('.testimonials__img');

        // If we have images
        if($images.length) {
          // Show the first img
          $images.eq(0).css('display', 'block');

          $orbit.on('slidechange.zf.orbit', function() {
            var $this        = $(this),
                $slides      = $this.find('.orbit-slide'),
                $activeSlide = $slides.filter('.is-active'),
                activeNum    = $slides.index($activeSlide) + 1,
                $activeImg   = $orbit.find('.testimonials__img[data-slide=' + activeNum + ']');

            // Show if the slide has an img
            if($activeImg.length) {
              $images.fadeOut().removeClass('is-active');
              $activeImg.fadeIn().addClass('is-active');
            } else {
              $images.fadeOut().removeClass('is-active');
            }
          });
        }
      });
    },

    valueInArray: function(value, arr) {
      return $.inArray(value, arr) !== -1;
    },

    filterControls: function(e) {
      var $active      = null,
          activeClass  = 'filter__item--active',
          activeGroups = [];

      e.preventDefault();

      // Toggle active class
      $(e.currentTarget).toggleClass(activeClass);

      // Create our array of activeGroups
      $active = s.$filter.find('.' + activeClass);

      // At least one filter is active
      if($active.length) {
        $active.each(function() {
          activeGroups.push($(this).data('group') );
        });
      }

      // Shuffle the grid
      s.$filterShuffle.shuffle('shuffle', function($el) {
        var data  = $el.data(),
            group = data.groups;

        if(activeGroups.length > 0) {
          // If the group is an array the element has multiple groups assigned
          if($.isArray(group)) {
            var array = [];

            // Loop through the group array
            $.each(group, function(index, value) {
              var found = false;

              // If the value matches an active group mark it as true
              if(!$.inArray(value, activeGroups)) {
                found = true;
              }

              array.push(found);
            });

            // If we find true in our array atleast 1 group has a match so we show the element
            if(SolentUI.valueInArray(true, array)) {
              return true;
            }
          } else if(SolentUI.valueInArray(group, activeGroups)) {
            return true;
          }
        } else {
          return true;
        }
      });
    },

    filterReset: function(e) {
      e.preventDefault();
      // Remove active class
      s.$filterControls.removeClass('filter__item--active');
      // Reset the grid
      s.$filterShuffle.shuffle('shuffle', 'all');
    },

    imgLiquid: function() {
      $('.imgLiquidFill').imgLiquid();

      // Refire equalizer on AJAX
      $(document).ajaxComplete( function() {

        // If input is checked
        var checked = $('.compare input[type=checkbox]');

        $(document).on('click', checked, function (){
          if ( checked.is(":checked") ) {
              $('.compare-bar').removeClass('hide');
          } else {
              // Do nothing
          }
        });

        $('.imgLiquidFill').imgLiquid();

      });
    },

    stickyNav: function() {
      var stickyNav = 478,
          stickyPinned = 805;

      // Get window scroll position
      s.$window.scroll(function() {
        var scroll = s.$window.scrollTop();

        if (scroll >= stickyNav) {
          s.$fix.addClass('is-active');
        } else {
          s.$fix.removeClass('is-active');
        }

        if (scroll >= stickyPinned) {
          s.$pinned.addClass('is-active');
        } else {
          s.$pinned.removeClass('is-active');
        }
      });

      // Calculate offset position on window resize
      s.$window.resize(function() {
        stickyNav = calculatesticky();
      });

      // Calculate offset position on window load
      s.$window.load(function() {
        stickyNav = calculatesticky();
      });

      // Calculate offset position by window width
      function calculatesticky() {
        if(s.$window.width() < 640) {
          return stickyNav;
        }

        if(s.$window.width() >= 640) {
          return 669;
        }
      }
    },

    progressBar: function() {
      $('#keyFigures').waypoint(function() {
        $('.progress').each(function(index) {
          var countValue = $(this).data('value'),
              countColour = $(this).data('color'),
              countWidth = $(this).data('width'),
              trailWidth = $(this).data('trail');

          var bar = new ProgressBar.Circle(this, {
            strokeWidth: countWidth,
            easing: 'easeInOut',
            duration: 1400,
            trailColor: '#f4f4f4',
            from: {
              color: countColour,
              width: countWidth
            },
            to: {
              color: countColour,
              width: countWidth
            },
            // Set default step function for all animate calls
            step: function(state, circle) {
              circle.path.setAttribute('stroke', state.color);
              circle.path.setAttribute('stroke-width', state.width);

              var value = Math.round(circle.value() * 100) + '%';
            }
          });
          bar.animate(countValue);
        });
        this.destroy()
      });
    },

    magnificPopup: function() {
      // Video
      $('.js-video').magnificPopup({
        midClick: true,
        removalDelay: 300,
        mainClass: 'mfp-fade',
        prependTo: $('body'),
        type: 'iframe',
        iframe: {
          patterns: {
            youtube: {
              index: 'youtube.com/embed/',
              id: 'embed/',
              src: '//www.youtube.com/embed/%id%?autoplay=1'
            },
            youtube_watch: {
              index: 'youtube.com/watch',
              id: 'v=',
              src: '//www.youtube.com/embed/%id%?autoplay=1'
            },
            youtube_short: {
              index: 'youtu.be/',
              id: 'youtu.be/',
              src: '//www.youtube.com/embed/%id%?autoplay=1'
            }
          }
        }
      });

      // Inline
      $('.js-inline').magnificPopup({
        midClick: true,
        removalDelay: 300,
        mainClass: 'mfp-fade',
        prependTo: $('#form1'),
        type: 'inline'
      });

      // Gallery
      $('.gallery__link').magnificPopup({
        type: 'image',
        gallery:{
          enabled:true
        }
      });

      $('.gallery__link1').magnificPopup({
        type: 'image',
        gallery:{
          enabled:true
        }
      });
    },

    filtering: function() {
      if(s.$filter.length) {
        // Filter: shuffle columns
        s.$filterShuffle.shuffle({
          speed: 250,
          itemSelector: '.column'
        });

        // Filter: controls
        s.$filterControls.on('click', SolentUI.filterControls);

        // Filter: reset
        s.$filter.find('.js-filter-reset').on('click', SolentUI.filterReset);

        // Filter: popup
        s.$filter.find('.js-filters-block').magnificPopup({
          midClick: true,
          removalDelay: 300,
          mainClass: 'mfp-fade mfp-no-spacing',
          prependTo: $('#form1'),
          type: 'inline',
          alignTop: true,
          fixedContentPos: true,
          callbacks: {
            open: function() {
              // Resize debounce if the screen is wider than 1023 we want to close the panel
              s.toggleFiltersPanel = SolentUI.debounce(function() {
                  if(s.$window.innerWidth() > 1023) {
                    $.magnificPopup.instance.close();
                  }
              }, 250);

              window.addEventListener('resize', s.toggleFiltersPanel);
            },
            close: function() {
              if(s.$window.innerWidth() > 1023) {
                // Remove the listener and set it back to null
                window.removeEventListener('resize', s.toggleFiltersPanel);
                s.toggleFiltersPanel = null;
              }

              // Show the panel
              $.magnificPopup.instance.content.removeClass('mfp-hide');
            }
          }
        });
      }
    },

    shuffleCols: function() {
      if(s.$shuffleCols.length) {
        // Init Shuffle columns
        s.$shuffleCols.shuffle({
          speed: 250,
          itemSelector: '.column',
          initialSort: { // Sort them by date created
            reverse: true,
            by: function($el) {
              return $el.data('date-created');
            }
          }
        });

        // Once all images have loaded trigger shuffle to re-calc the positions
        s.$shuffleCols.imagesLoaded( function() {
          s.$shuffleCols.shuffle('update');
        });
      }
    },

    alternateSection: function() {
      $('.courseSection').each(function() {
        var $this = $(this);

        if($this.prev('.courseSection').length) {
          var lastItem = $this.prev('.courseSection').children(':last-child').hasClass('section--lightgrey');

          // If the last item had the grey class apply to even else apply to odd
          if(lastItem) {
            $this.children(':nth-child(odd)').addClass('section--white');
            $this.children(':nth-child(even)').addClass('section--lightgrey');
          } else {
            $this.children(':nth-child(odd)').addClass('section--lightgrey');
            $this.children(':nth-child(even)').addClass('section--white');
          }
        } else {
          $this.children(':nth-child(odd)').addClass('section--white');
          $this.children(':nth-child(even)').addClass('section--lightgrey');
        }
      });
    },

    menuToggle: function() {
		  $('.menu-search-toggle').click(function(event){
        event.preventDefault();

        $(this).toggleClass('menu-search-toggle--active');
        $('.menu-search').toggleClass('menu-search--active');
      });

      $('.nav--secondary__toggle, .nav--tertiary__toggle').click(function() {
		    event.preventDefault();

        $('.nav--secondary ul').toggleClass('show');
		  });

      $('.nav--secondary__item a').click(function() {
			 $('.nav--secondary ul').removeClass('show').addClass('hide');
		  });
    },

    compareBar: function() {
      var checked = $('.compare input[type=checkbox]');

      $(document).on('click', checked, function (){
        if (checked.is(':checked')) {
          $('.compare-bar').removeClass('hide');
        } else {
          $('.compare-bar').addClass('hide');
        }
      });
    },

    smoothScrollTop: function() {
      $('.skip-to-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, 1000);
      });
    },

    groupFilter: function() {
      $('.groupfilter__item').click(function(e) {
        // Prevent linking
        e.preventDefault();

        var activeClass = 'groupfilter__item groupfilter__item--active',
            genericClass = 'groupfilter__item';

        // Collections
        var elements = document.getElementsByClassName('groupfilter__item');

        // Data attributes
        var selectedFilter = $(this).attr('data-filter'),
            current = $(this).attr('data-current');

        //loop through the elements in collection
        for (var i = 0; i < elements.length; i++) {
           var groupItemFilter = $(elements[i]).attr('data-filter')

           // Then compare the filter of the onclick element with that from the collection
           // Any matches will apply the active class
           if(selectedFilter == groupItemFilter) {
             elements[i].className = activeClass;
           } else { // Otherwise assign the generic class
             elements[i].className = genericClass;
           }
        }

        // AJAX
        $('#NewsFeed').load('?filter=' + selectedFilter +  ' #NewsFeed > *' );
      });
    },

    stickyColumn: function() {
      $('.sticky_column').stick_in_parent();
    },

    moveNewsSidebar: function() {
      var $sidebar = $('.js-news-sidebar');

      // If the sidebar doesn't exist do nothing
      if(!$sidebar.length) {
        return;
      }

      var $from      = $sidebar.parent();
      var $to        = $($sidebar.data('move-to'));
      var ranMobile  = false;
      var ranDesktop;

      if(s.$window.innerWidth() > 1023) {
        ranDesktop = true;
      } else {
        ranDesktop = false;
      }

      // Resize debounce if the screen is wider than 1023 move to desktop position else move to mobile position
      var move = SolentUI.debounce(function() {
        if(s.$window.innerWidth() > 1023) {
          if(!ranDesktop) {
            $sidebar.show(); // Show the sidebar
            $to.html('');    // Remove the cloned version

            ranDesktop = true;
            ranMobile = false;
          }
        } else {
          if(!ranMobile) {
            $sidebar.hide();                       // Hide sidebar
            $sidebar.clone().appendTo($to).show(); // Show new sidebar

            ranMobile = true;
            ranDesktop = false;
          }
        }
      }, 250);

      window.addEventListener('resize', move);

      // Trigger on page load
      move();
    },

    bindUIActions: function() {
      // Remove border from a linked image
      $('img').parent('a').css("border-bottom", "none");

      // Add a caption to news images
      $('.imgCaption').each(function() {
        var captText = $(this).attr('alt'),
            floatClass = '';

        if ($(this).hasClass('usr-float-left')){
          floatClass = 'usr-float-left';
        }

        if ($(this).hasClass('usr-float-right')){
          floatClass = 'usr-float-right';
        }

        $(this).wrap('<figure class="news__img"></figure>');
        $(this).after('<figcaption class="img-caption img-caption--article img-caption--thumb"></figcaption>');
        $(this).next().append(captText);
        $(this).parent('.news__img').addClass(floatClass);
      });

      // Toggle hidden content
      $('.button-view-more').click(function(event) {
        event.preventDefault();

        $(this).prev('.hide').toggle();
        $(this).toggleClass('active');

        if ($.trim($(this).text()) === 'View more') {
          $(this).text('View less');
        } else {
          $(this).text('View more');
        }
      });

      // Prevent orbit nav submitting form
      $('.orbit-bullets button').click(function(e) {
        e.preventDefault();
      });

      // Cookie bar
      $.cookieBar();

    },

    bindWindowActions: function() {
      // Google Map
      if ($('#map__canvas').length) {
        google.maps.event.addDomListener(window, 'load', SolentUI.googleMaps);
      }
    }
  };
})(jQuery);

// Form submit
function ContensisSubmitFromTextbox(event, lala) {
  var btn = document.getElementById(lala)

  if (document.all) {
    if (event.keyCode == 13) {
      event.returnValue = false;
      event.cancel = true;
      killEvent(event); // [Added BJ 30/01/2012 for IE9]
      btn.click();
    }
  }
  else if (document.getElementById) {
    if (event.which == 13) {
      event.returnValue = false;
      event.cancel = true;
      killEvent(event); // [Added iG 18/01/2012]
      btn.click();
    } else {
        // Do nothing
    }
  }
  else if (document.layers) {
    if (event.which == 13) {
      event.returnValue = false;
      event.cancel = true;
      btn.click();
    } else {
        // Do nothing
    }
  }
}

function killEvent(eventObject) {
  if (eventObject && eventObject.stopPropagation) {
    eventObject.stopPropagation();
  }
  if (window.event && window.event.cancelBubble) {
    window.event.cancelBubble = true;
  }
  if (eventObject && eventObject.preventDefault) {
    eventObject.preventDefault();
  }
  if (window.event) {
    window.event.returnValue = false;
  }
}

$(document).ready(function() {
  SolentUI.init();
});

// ACCORDIONS
$('.acc__content').hide();
$('.acc__tgr').click(function (e) {
    e.preventDefault();
    $(this).toggleClass('acc__tgr--active');
    $(this).next('.acc__content').slideToggle();
});
